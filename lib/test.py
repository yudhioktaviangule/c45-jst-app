import pdb
import os
from c45.c45 import C45
from c45.jsonFile import jsonFile
pdata = os.path.dirname(os.path.realpath(__file__))+'/c45/data-training/data.csv'
pname = os.path.dirname(os.path.realpath(__file__))+'/c45/iris.names'
print(pdata)
c45 = C45(pdata,pname)
c45.fetchData()
c45.preprocessData()
c45.generateTree()
c45.printTree()
js = jsonFile(c45.tree)
js.convertToDict()
jsonx = js.jsonDict
print(jsonx)




