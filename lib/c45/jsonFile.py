import json

class jsonFile:
    def __init__(self,tree,attributes):
        self.tree = tree
        
        self.attributes = attributes
        self.jsonDict = self.convertToDictonaryC45(self.tree,None)


    def convertToDict(self):
        children = self.createChildren(self.tree.children)
        self.jsonDict = {
            "label":self.tree.label,
            "isLeaf":self.tree.isLeaf,
            "thresold":self.tree.threshold,
            "children":children

        }
    def createChildren(self,nodes):
    
        dicts = []
        for child in nodes :
            isHaveChild = len(child.children)
            if isHaveChild > 0:
                dicts_child =  self.createChildren(child.children)
            else:
                dicts_child = None
            dicts.append({
                "label":child.label,
                "isLeaf":child.isLeaf,
                "thresold":child.threshold,
                "children":dicts_child
            })
        return dicts

        
    def convertToDictonaryC45(self,nodes,nodp):      
        nod = {}
        if type(nodes)!=list:
            if not nodes.isLeaf:
                if nodes.threshold is None:
                    for index,child in enumerate(nodes.children):
                        
                        if child.isLeaf:
                            nod = {
                                "name":'IF '+nodes.label+' = '+ self.attributes[index],
                                "title":children.label,
                                "thresold":nodes.threshold,
                                "isLeaf":nodes.isLeaf,
                                "children":None
                            }
                        else:
                            children = self.convertToDict(child.children)
                            nod = {
                                "name":'IF '+nodes.label+' = '+ self.attributes[index],
                                "title":'IF '+nodes.label+' = '+ self.attributes[index],
                                "threshold":nodes.threshold,
                                "isLeaf":nodes.isLeaf,
                                "children":children
                            }
                else:
                    leftChild = nodes.children[0]
                    rightChild = nodes.children[1]
                    nod =[]
                    if leftChild.isLeaf:
                        nodl = {
                            "name":'IF '+nodes.label+' <= '+str(nodes.threshold),
                            'title':leftChild.label,
                            'threshold':nodes.threshold,
                            "isLeaf":nodes.isLeaf,
                            
                        }
                    else:
                        children = self.convertToDictonaryC45(leftChild.children,leftChild)
                        nodl = {
                            "name":'IF '+nodes.label+' <= '+str(nodes.threshold),
                            "title":'IF '+nodes.label+' <= '+str(nodes.threshold),
                            "isLeaf":nodes.isLeaf,
                            'threshold':nodes.threshold,
                            'children':children
                        }
                    
                    if rightChild.isLeaf:
                        
                        nodr = {
                            "name":'IF '+nodes.label+' > '+str(nodes.threshold),
                            'title':rightChild.label,
                            "isLeaf":nodes.isLeaf,
                            'threshold':nodes.threshold,
                            
                        }
                    else:
                        children = self.convertToDictonaryC45(rightChild.children,rightChild)
                        nodr = {
                            "name":'IF '+nodes.label+' > '+str(nodes.threshold),
                            "title":'IF '+nodes.label+' > '+str(nodes.threshold),
                            "isLeaf":nodes.isLeaf,
                            'threshold':nodes.threshold,
                            'children':children
                        }
                    nod = [nodl,nodr]
            
        else:
            nod=[]
            
            leftChild = nodes[0]
            print('NODE 0',nodes[0])
            rightChild = nodes[1]
            nod =[]
            if leftChild.isLeaf:
                nodl = {
                    "name":'IF '+nodp.label+' <= '+str(nodp.threshold),
                    'title':leftChild.label,
                    'threshold':nodp.threshold,
                    "isLeaf":nodp.isLeaf
                }
            else:
                children = self.convertToDictonaryC45(leftChild.children,leftChild)
                nodl = {
                    "name":'IF '+nodp.label+' <= '+str(nodp.threshold),
                    "title":'IF '+nodp.label+' <= '+str(nodp.threshold),
                    "isLeaf":nodp.isLeaf,
                    'threshold':nodp.threshold,
                    'children':children
                }
            
            if rightChild.isLeaf:
                
                nodr = {
                    "name":'IF '+nodp.label+' > '+str(nodp.threshold),
                    'title':rightChild.label,
                    "isLeaf":nodp.isLeaf,
                    'threshold':nodp.threshold
                    
                }
            else:
                children = self.convertToDictonaryC45(rightChild.children,rightChild)
                nodr = {
                    "name":'IF '+nodp.label+' > '+str(nodp.threshold),
                    "title":'IF '+nodp.label+' > '+str(nodp.threshold),
                    "isLeaf":nodp.isLeaf,
                    'threshold':nodp.threshold,
                    'children':children
                }
            nod = [nodl,nodr]
        return nod
      
         
        
                    
            
                



                    
                
