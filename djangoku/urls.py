
from django.contrib import admin
from django.urls import path
from c45app.views import upload
from c45app.views import c45_process
urlpatterns = [
    path('admin/', admin.site.urls),
    path('c45/upload', upload),
    path('c45/process', c45_process),
    
]
