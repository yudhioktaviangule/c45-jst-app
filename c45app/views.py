import os
import io
import csv
import json
from django.shortcuts import render
from django.http import HttpResponse
from lib.c45.c45 import C45
from lib.c45.jsonFile import jsonFile
from django.views.decorators.csrf import csrf_exempt
from c45app.forms.forms import UploadFileForm


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
def upload(Request):
    konteks = {
        'title':'C45',
        'description':'Upload File',
        'pathUri':Request.build_absolute_uri("process")
    }
    return render(Request,'index.html',konteks)

@csrf_exempt
def c45_process(request):
    handle_uploaded_file(request.FILES['file'])
    pathToNames = os.path.join(BASE_DIR,"static/docs/iris.names")
    pathToCsv = os.path.join(BASE_DIR,"static/docs/dataset.csv")
    c = C45(pathToCsv, pathToNames)
    c.fetchData()
    c.preprocessData()
    c.generateTree()
    tree = c.tree
    attr = c.attributes
    #print(tree)
    dictJ = jsonFile(tree,attr)
    
    jsondict = dictJ.jsonDict
    jsondata = json.dumps(jsondict)
    jsonFinal = json.loads(jsondata)
    return HttpResponse(jsondata)

def handle_uploaded_file(f):
    pathToUploaded = os.path.join(BASE_DIR,'static/docs/dataset.csv')
    with open(pathToUploaded,'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)